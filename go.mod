module url_shortener

go 1.21.3

require (
	github.com/fatih/color v1.16.0
	github.com/sethvargo/go-envconfig v1.0.0
)

require (
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.20 // indirect
	github.com/mattn/go-sqlite3 v1.14.22
	golang.org/x/sys v0.14.0 // indirect
)
