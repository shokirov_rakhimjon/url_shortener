package main

import (
	"context"
	"fmt"
	"log/slog"
	"os"
	"url_shortener/config"
	"url_shortener/internal/lib/logger/sl"
	"url_shortener/internal/sqlite"

	envconfig "github.com/sethvargo/go-envconfig"
)

const (
	envLocal = "local"
	envDev   = "dev"
	envProd  = "prod"
)

func main() {
	//	TODO: init config: cleanenv
	var cfg config.Config
	if err := envconfig.Process(context.TODO(), &cfg); err != nil {
		panic(err)
	}

	//	TODO: init logger: slog
	log := setUpLogger(cfg.Env)
	log.Info("starting url_shortener env", slog.String("user", cfg.Env))

	//	TODO: init storage: sqlite
	storage, err := sqlite.New(cfg.StoragePath)
	if err != nil {
		log.Error("failed to init storage", sl.Err(err))
		os.Exit(1)
	}
	alias, err := storage.SaveURL("https://google.com", "google")
	if err != nil {
		log.Error("failed to save url", sl.Err(err))
	}
	fmt.Println("alias: ", alias)
	//	TODO: init router: chi-router, chi-render

	//	TODO: run server: HTTP

}

func setUpLogger(env string) *slog.Logger {
	var log *slog.Logger
	switch env {
	case envLocal:
		log = slog.New(
			slog.NewTextHandler(os.Stdout, &slog.HandlerOptions{Level: slog.LevelDebug}),
		)
	case envDev:
		log = slog.New(
			slog.NewJSONHandler(os.Stdout, &slog.HandlerOptions{Level: slog.LevelDebug}),
		)
	case envProd:
		log = slog.New(
			slog.NewJSONHandler(os.Stdout, &slog.HandlerOptions{Level: slog.LevelInfo}),
		)
	}
	return log
}
