package config

import (
	"time"
)

type Config struct {
	Env         string      `env:"ENVIRONMENT,default=test"`
	StoragePath string      `env:"STORAGE_PATH"`
	HTTPServer  *HTTPServer `env:",prefix=HTTP_SERVER_"`
}

type HTTPServer struct {
	Address     string        `env:"ADDRESS, default=localhost:8080"`
	Timeout     time.Duration `env:"TIMEOUT, default=4s"`
	IdleTimeout time.Duration `env:"IDLE_TIMEOUT, default=60s"`
	User        string        `env:"USER"`
	Password    string        `env:"PASSWORD"`
}
